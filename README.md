# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### What is this repository for? ###

The objectives of this mini-project are:

  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

### What do I need?  Where will it work? ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. A Linux virtual machine may work, but our experience has not been good; you may want to test on shared server ix-dev.

* You will need Python version 3.4 or higher. 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

* Windows 10 note: The new Windows bash on ubuntu looks promising. If you are running Windows 10, please give this a try and let me know if the Ubuntu/bash environment is suitable for CIS 322 develpment.
 
### Author: Newton Blair, nblair@uoregon.edu ###

### Description of Software ###
This program should be able to take a request for a file from a local server and handle either sending the server the requested file(s), if the file(s) are in the DOCROOT path, or returns various errors.
The errors should be handled as followed:
1) If it is requesting a file with the symbols // .. or ~ within it then it serves a 403 forbidden error.
2) If it is requesting a valid file name but the file can not be located in the DOCROOT path then it serves a 404 (not found) error.
3) If the request is not a GET request, then it serves a 401 not implemented error.

  
   

 
